package com.example.wakelock;

import android.app.Activity;
import android.app.KeyguardManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

public class AlarmScreenOnReceiver extends BroadcastReceiver {
    private PowerManager mPowerManager;
    private PowerManager.WakeLock mWakeLock;

    @Override
    public void onReceive(Context context, Intent intent) {
        mPowerManager = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        //当系统到我们设定的时间点的时候会发送广播，执行这里
        Toast.makeText(context, "AlarmScreenOnReceiver==", Toast.LENGTH_SHORT).show();
        Log.v("qqq", "AlarmScreenOnReceiver==");
        checkScreenOn(context);
    }

    /**
     * 亮屏
     */
    public void checkScreenOn(Context context) {
        mWakeLock = mPowerManager.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP, "wakelock:" + this.getClass().getSimpleName());
//        mWakeLock = mPowerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, this.getClass().getName());//持有唤醒锁
        KeyguardManager km = (KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE);
        KeyguardManager.KeyguardLock kl = km.newKeyguardLock("name");
        kl.disableKeyguard();
        mWakeLock.acquire();
        mWakeLock.release();

    }
}
