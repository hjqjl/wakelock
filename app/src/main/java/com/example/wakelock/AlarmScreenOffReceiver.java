package com.example.wakelock;

import android.app.admin.DevicePolicyManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

public class AlarmScreenOffReceiver extends BroadcastReceiver {
    private DevicePolicyManager policyManager;
    private ComponentName adminReceiver;

    @Override
    public void onReceive(Context context, Intent intent) {
        adminReceiver = new ComponentName(context, ScreenOffAdminReceiver.class);
        policyManager = (DevicePolicyManager) context.getSystemService(Context.DEVICE_POLICY_SERVICE);
        //当系统到我们设定的时间点的时候会发送广播，执行这里
        Toast.makeText(context, "AlarmScreenOffReceiver++++", Toast.LENGTH_SHORT).show();
        Log.v("qqq", "AlarmScreenOffReceiver++++");
        checkScreenOff();
    }

    /**
     * 熄屏
     */
    public void checkScreenOff() {
        boolean admin = policyManager.isAdminActive(adminReceiver);
        if (admin) {
            policyManager.lockNow();
        } else {
//            showToast("没有设备管理权限");
            Log.v("qqq", "没有设备管理权限");
        }
    }

//    private void showToast(String Str) {
//        Toast.makeText(this, Str, Toast.LENGTH_SHORT).show();
//    }
}
